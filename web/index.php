<?php

/// Exit early if php requirement is not satisfied.
if (version_compare(PHP_VERSION, '7.0.0', '<')) {
    die('This version requires PHP 7.0 or above');
}

// Set up the application for the Frontend
call_user_func(function () {
    require __DIR__ . '/../vendor/autoload.php';
    (new \Leonmrni\FluidStyleguide\Application(__DIR__))->run();
});